
TESTWALLET := $(shell pwd)/wallet


build:
	docker.io build -t ferranp/bitcoin-testnet .

run:
	docker.io run -d -v ${TESTWALLET}:/.bitcoin ferranp/bitcoin-testnet

runtest:
	docker.io run -i -t -v ${TESTWALLET}:/.bitcoin ferranp/bitcoin-testnet
